// TODO 
// 1. fix the pass function first
// end game when all cards in hand have faceup == true
// 1.1 need to open all cards for all players
// 1.2 update points

// 2. add 10 points to others for 2 adjacent pairs

// 3. fix pass function
// 3.1. DONE one option: no need for pass.
// 3.2. DONE when only one card is facedown, 
// 3.3. DONE instead of forcing user to flip after discard, allow to simply discard
// 3.4. on Last card - I should have the option to end the game by flipping my last card
// 3.5. when some player ends game, all other players should get the option to improve their game

// DONE Add JOKER
// DONE Suit and rank could both be same.

// DONE The way to add a new player should be more dynamc


// demo data
var data = {
  name: 'My Tree',
  children: [
    { name: 'hello' },
    { name: 'wat' },
    {
      name: 'child folder',
      children: [
        {
          name: 'child folder',
          children: [
            { name: 'hello' },
            { name: 'wat' }
          ]
        },
        { name: 'hello' },
        { name: 'wat' },
        {
          name: 'child folder',
          children: [
            { name: 'hello' },
            { name: 'wat' }
          ]
        }
      ]
    }
  ]
}

// ♠ U+2660
// ♥ U+2665
// ♦ U+2666 
// ♣ U+2663
JOKER = {
  rank : 'Jo',
  suit : 'joker'
}

var symbol = {
  "hearts"    : '\u2665',
  "spades"    : '\u2660',
  "clubs"     : '\u2663',
  "diamonds"  : '\u2666',
  'joker'     : '\u263B',
  
}

var suits = ["hearts", "spades", "diamonds", "clubs"];
var ranks = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10","J", "Q", "K"];
var scores = {
    "A":-1,
    "2":2,
    "3":3,
    "4":4,
    "5":5,
    "6":6,
    "7":7,
    "8":8,
    "9":9,
    "10":10,
    "J":10,
    "Q":10,
    "K":0,
    "Jo":0,
  }

  MOVE_REPLACE = 0;
  MOVE_PICK_NEW = 1;
  MOVE_DISCARD_FLIP = 2;
  MOVE_DISCARD_ONLY = 3;
  MOVE_OPEN_LAST_CARD = 4;

  VALID_MOVES = {
    0 : "MOVE_REPLACE", 
    1 : "MOVE_PICK_NEW",
    2 : "MOVE_DISCARD_FLIP",
    3 : "MOVE_DISCARD_ONLY",
    4 : "MOVE_OPEN_LAST_CARD"
  }

  STRATEGY_OPEN_FIRST = 0;
  STRATEGY_REPLACE_SMALLER = 1;

  HAND_SIZE = 8;

function Card(suit, rank){
  this.suit = suit;
  this.rank = rank;
  this.card = rank+symbol[suit];
  this.faceup = false;
  this.swapMode = false;
  this.flipMode = false;
  this.player = "";
  this.pos = -1;
  this.url = "cards/"+this.rank+this.suit.charAt(0).toUpperCase() + ".svg";
}
var fulldeck = [];
var remainder = [];
var discarded = [];
var picked = {};
var shared = {};
shared.picked = picked;
shared.discarded = discarded;
shared.player = "1";
shared.swapMode = false;
shared.flipMode = false;
shared.gameOver = false;
shared.joker = '\u263B';
shared.players = [];
shared.playerMap = {};
shared.isLastMove = false;
shared.lastMovePlayedBy = {};
shared.player = null;
shared.turnLog = [];
shared.nextQueue = [];
shared.gameEndedBy ="";
shared.doublePairs = {};
shared.doublePairIndexPatterns = []
shared.scores = {};

function setDoublePairIndexPatterns(max){
    var offset = max/2;
    for(x=0;x<offset-1;x++){
      var base = x;
      // console.log("[%o, %o],[%o, %o]",base,base+offset,base+1,base+1+offset)
      var indexPattern = [base,base+offset,base+1,base+1+offset];
      // indexPattern.sort(function(a,b){
      //   return a - b;
      // })
      shared.doublePairIndexPatterns.push(indexPattern);
    }
}
setDoublePairIndexPatterns(HAND_SIZE);


DECK_COUNT = 2;
for(i=0;i<DECK_COUNT;i++){
suits.forEach(suit => {
  ranks.forEach( rank => {
    fulldeck.push(new Card(suit, rank));
  })
})
fulldeck.push(new Card(JOKER.suit, JOKER.rank))
fulldeck.push(new Card(JOKER.suit, JOKER.rank))  
}


function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

shuffle(fulldeck);

console.log(fulldeck);


// define the item component
// Vue.component('item', {
//   template: '#item-template',
//   props: {
//     model: Object
//   },
//   data: function () {
//     return {
//       open: false
//     }
//   },
//   computed: {
//     isFolder: function () {
//       return this.model.children &&
//         this.model.children.length
//     }
//   },
//   methods: {
//     toggle: function () {
//       if (this.isFolder) {
//         this.open = !this.open
//       }
//     },
//     changeType: function () {
//       if (!this.isFolder) {
//         Vue.set(this.model, 'children', [])
//         this.addChild()
//         this.open = true
//       }
//     },
//     addChild: function () {
//       this.model.children.push({
//         name: 'new stuff'
//       })
//     }
//   }
// })


Vue.component('card', {
  template: '#card-template',
  props: {
    cardmodel: Object,
    player: Object,
    hand: Array,
  },
  data: function () {
    return {
      open: false,
      }
  },
  computed: {
    
  },
  methods: {
    clicked : function(){
      if(this.cardmodel.flipMode){
        if(this.cardmodel.faceup){
          return;
        }
        this.flipMe();
      }else{
        if(this.cardmodel.swapMode){
          // this.swapMe();
          var pos = this.cardmodel.pos;
          this.player.swapCard(this.cardmodel);
          // this.cardmodel = this.player.hand[pos];          
        }
      }
    },
    flipMe: function () {
      if(!this.cardmodel.flipMode){
        return;
      }
      console.log("flipping")
      bus.$emit('exitFlipMode',this.cardmodel);
      this.cardmodel.faceup=true;
      bus.$emit('flipCompleted',this.cardmodel);
    },
    // swapMe: function(){
    //   if(!this.model.swapMode){
    //     return;
    //   }
    //   console.log("swapping")
    //   bus.$emit('exitSwapMode', this.model);
      
    //   console.log(this.model);
    //   if(shared.picked.hasOwnProperty('card')){
    //     shared.picked.player = this.model.player;
    //     var a = JSON.stringify(this.model);
    //     var b = JSON.stringify(shared.picked);
    //     this.model = JSON.parse(b);
    //     discarded.pop();
    //     discarded.unshift(JSON.parse(a));
    //     discarded[0].faceup = true;
    //     shared.picked = {};
    //   }else{
    //     discarded[0].player = this.model.player;
    //     var a = JSON.stringify(this.model);
    //     var b = JSON.stringify(discarded[0]);
    //     this.model = JSON.parse(b);
    //     discarded.pop();
    //     discarded.unshift(JSON.parse(a));
    //     discarded[0].faceup = true;
    //   }

    //   bus.$emit('swapCompleted' ,{
    //     in: this.model,
    //     out : discarded[0],
    //     model : this.model,
    //   })

    // },

  },
  mounted : function(){
    self = this;
    
    // bus.$on('swapCompleted', function(d){
    //   console.log('on swap completed %o', d.model.player);
    //   console.log('player %o', self.id);
    //   if(d.model.player != self.player.id){
    //     console.log("not my event");
    //     return;
    //   }

    //   console.log(self.hand);
    //   console.log("added " + d.in.card);
    //   console.log("removed " + d.out.card);
    //   self.hand.forEach(function(s, ind, arr){
    //     if(s.card == d.out.card){
    //       // arr[ind]= d.in;
    //       this.model = d.in;
    //     }
    //   })

    // });

  }
})



Vue.component('hand', {
  template: '#hand-template',
  props: {
    model2: Array,
    player: Object,
  },
  data: function () {
    return {
      open: false
    }
  },
  computed: {
    
  },
  methods: {
    logObj : function(x){
      console.log("yo");
      console.log(x);
      return "yo"
    }
  }
})

Vue.component('player', {
  template: '#player-template',
  props: {
    model: Object
  },
  data: function () {
    return {
      open: false,
      shared: shared,
    }
  },
  computed: {
    
  },
  methods: {
  }
})

// boot up the demo
// var demo = new Vue({
//   el: '#demo',
//   data: {
//     treeData: data
//   }
// })

var game = new Vue({
  el: '#game',
  data : {
    shared : shared,
    fulldeck: fulldeck,
    message : "",
  },
  methods :{
    setPicked : function(){
      var self= this;
      for (x=0; x<fulldeck.length; x++){
        var c = fulldeck[x];
        if(c.card == self.message){
          shared.picked = c;
          break;
        } 
      }
    },
    clearScores : function(){
      localStorage.removeItem('golf-game-scores');
    }
  },
  created:function(){
    
  },
  mounted : function(){
    updateScores();
      // self = this;
      // this.scores = [];

      // var x = localStorage.getItem('golf-game-scores');

      // if(x){
      //   this.scores = JSON.parse(x);
      //   this.total = {};
      //   var min = "";

      //   this.scores.forEach(function(s){
      //     for(player in s){
      //       if(self.total[player]){
      //         self.total[player] += s[player];
      //       }else{
      //         self.total[player] = s[player];
      //       }
      //     }
      //   })
      // }

  }
})

function updateScores(){

  var x = localStorage.getItem('golf-game-scores');

      if(x){
        var scores = JSON.parse(x);
        var total = {};
        var min = "";

        scores.forEach(function(s){
          for(player in s){
            if(total[player]){
              total[player] += s[player];
            }else{
              total[player] = s[player];
            }
          }
        })

        Vue.set(shared, 'scores', scores);
        Vue.set(shared, 'playerTotal', total);
      }
}

var players = new Vue({
  el: '#players',
  data: {
    shared : shared,
    discarded : shared.discarded
  }
})

// #bus
bus = new Vue();

bus.$on('endgame', function(){
  var leastPointsScoredBy = [];
  var leastPoints = -999;
  
  var points = shared.players.map(p=> { return {'id' : p.id, 'score' : p.countPoints()}});

  var min = Math.min(...points.map(x => x.score));

  var winners = points.filter( x => x.score == min);

  winners.forEach((w,i,arr) => {
    shared.playerMap[w.id].setWinner();
  });

})

bus.$on('discarded', function(){
      console.log('~~~~~~~~~~~~~~~ discarded');
      if(fulldeck.length <= 10){
        var moveBackToFullDeck = discarded.splice(2);
        shuffle(moveBackToFullDeck);
        moveBackToFullDeck.forEach((c,i,arr) => {
          var x = arr[i];
          x.player = "";
          x.faceup = false;
          x.swapMode = false;
          x.flipMode = false;
          x.player = "";
          x.pos = -1;
          arr[i] =x;
        })
        Array.prototype.push.apply(fulldeck, moveBackToFullDeck);
      }
    });

bus.$on('swapCompleted', function(x){
    var lastSwapped = localStorage.getItem('lastSwapped');

    lastSwapped = x.out;

    localStorage.setItem('lastSwapped', JSON.stringify(lastSwapped));

})

    
bus.$on('gameover', function(){
  var x = localStorage.getItem('golf-game-scores');
  var scores;

  if(x){
    scores = JSON.parse(x);
  } 

  if(!scores){
    scores = [];
  }
  // var thisGameScore = shared.playerMap.map(p => { 
  //   return { 'id' : p.id, 
  //             'score' : p.countPoints
  //           }
  //   });

  var thisGameScore = {};

  for ( pid in shared.playerMap){
    var p = shared.playerMap[pid];
    var pscore = p.countPoints();
    thisGameScore[pid] =  pscore;
  }
  scores.push(thisGameScore);
  localStorage.setItem('golf-game-scores', JSON.stringify(scores));

  updateScores();
})

queue = new Vue({
  data: {
    _queue: []
  },
  methods :{
    add : function(x){this.$data._queue.push(x)},
    consume : function(x){return this.$data._queue.pop()}
  },
  created : function(){
    this.loop = setInterval((function(self){
         return function(){
          // console.log("123 - " + self.$data._queue.length);
          
          console.log("123 - " + self.$data._queue.length);
          var nextId = self.consume();
          console.log("next player " + nextId);
            if(nextId && nextId.length > 0){
              var isHuman = shared.playerMap[nextId].human; 
              console.log("%o isHuman ? %o", nextId, isHuman)
              if(!isHuman){
                console.log("autoplay next move for" + nextId); 
                shared.players.forEach(p => {if (p.id == nextId){p.playNextMove()}});
              }else{
                console.log("DO NOT auto play for " + nextId);            
              }  
            }
          }
        })(this), 1500);
  }
});


var fullDeck = new Vue({
  el: '#full-deck',
  data: {
    deck: fulldeck,
    picked:picked
  }
})

// var _picked = new Vue({
//   el: '#pickedSection',
//   data: {
//     picked: picked,
//     shared : shared,
//   },
//   created : function(){
//     bus.$on('picked', function(p){
//       console.log(p)
      
//     });
//   }
// })

// var _discarded = new Vue({
//   el: '#discardedSection',
//   data: {
//    shared : shared,
//   }
// })

var human = true;
var computer = false;
var player1 = createPlayer("player1", human);
var player2 = createPlayer("player2", computer);
var player3 = createPlayer("player3", computer);
var player4 = createPlayer("player4", computer);
var player5 = createPlayer("player5", computer);
shared.players.push(player1);
shared.playerMap[player1.id] = player1;
shared.players.push(player2);
shared.playerMap[player2.id] = player2;
shared.players.push(player3);
shared.playerMap[player3.id] = player3;
shared.players.push(player4);
shared.playerMap[player4.id] = player4;
shared.players.push(player5);
shared.playerMap[player5.id] = player5;

// shared.player = shared.players[0].id;

function createPlayer(id, isHuman){

  return new Vue({
    el: "#"+id,
    data: {
      id : id,
      //hand : getNewHand(8),
      hand : getNewHandForPlayer(HAND_SIZE, id),
      cols : 4,
      self : this,
      winner : false,
      gameEndedByMe : false,
      human: isHuman,
    },
    computed: {
      
    },
    methods:{
      setWinner : function(){
        this.winner = true;
      },
      countFaceUp : function(){
        var count=0;
        this.hand.forEach(function(c){
          if(c.faceup){
            count++;
          }
        })
        return count;
      },
      getFaceUpCards : function(){
        return this.hand.filter(c => c.faceup);
      },
      getFaceDownCards : function(){
        return this.hand.filter(c => !c.faceup);
      },
      countFaceDown : function(){
        var count=0;
        this.hand.forEach(function(c){
          if(!c.faceup){
            count++;
          }
        })
        return count;
      },
      endGame : function(){
        this.hand.forEach(function(c){
          c.faceup = true;
        })
        this.countPoints();
        // bus.$emit('calcNextMove', 'calcNextMove');
      },

      pickNewCard : function (){
        console.log("orig %o", picked);
        picked = fulldeck.pop();
        picked.faceup = true;
        console.log("new %o", picked);
        bus.$emit('picked', 'picked');
        shared.picked = picked;
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      discardPickedAndFlip : function (){
        discarded.unshift(picked);
        console.log("discarded %o", discarded);
        picked = null;
        shared.picked = {};
        bus.$emit('discarded', 'discarded');
        this.enterFlipMode();
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      autoDiscardPicked : function(){
        this.discardPicked();
      },
      discardPicked : function (){
        discarded.unshift(shared.picked);
        console.log("discarded %o", discarded);
        picked = null;
        shared.picked = {};
        bus.$emit('discarded', 'discarded');
        //switchPlayer();
        this.endTurn();
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      autoOpenLastCard : function(){
        this.openLastCard();
      },
      openLastCard : function(){
        // @assume: there is only one facedown card in hand
        this.hand.forEach(function(c){
          if(!c.faceup){
            c.faceup = true;
          }
          bus.$emit('openLastCard', 'openLastCard');
          bus.$emit('calcNextMove', 'calcNextMove');
        })

        ////////////////////////
        /////// warning ////////
        ////////////////////////
        // make sure to end turn before marking as last move
        this.endTurn();
        ////////////////////////////////
        this.playedLastMove = true;
        shared.lastMovePlayedBy[this.id] = true;
        shared.isLastMove = true;
        bus.$emit('endTurn', 'endTurn');
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      enterFlipMode : function(){
        this.$data.hand.forEach(function(c){
          c.flipMode = true;
        });
        // bus.$emit('enterFlipMode', 'enterFlipMode');        
        shared.flipMode = true;
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      exitFlipMode : function(){
        console.log("exiting flipmode")
        this.$data.hand.forEach(function(c){
          c.flipMode = false;
        });
        shared.flipMode = false;
        // bus.$emit('exitFlipMode', 'exitFlipMode');
        
        console.log("countfaceup " + this.countFaceUp());
        console.log("countfacedown " + this.countFaceDown());
        bus.$emit('calcNextMove', 'calcNextMove');
      },
      enterSwapMode : function(){
        this.hand.forEach(function(c){
          c.swapMode = true;
        });
        shared.swapMode = true;
        // bus.$emit('enterSwapMode', 'enterSwapMode');
        console.log("entered swap mode");
        bus.$emit('calcNextMove', 'calcNextMove');
      },

      exitSwapMode : function(){
        console.log("exiting swapmode")
        this.hand.forEach(function(c){
          c.swapMode = false;
        });
        shared.swapMode = false;
        // bus.$emit('exitSwapMode', 'exitSwapMode');
        
        console.log("countfaceup " + this.countFaceUp());
        console.log("countfaceup " + this.countFaceDown());
        bus.$emit('calcNextMove', 'calcNextMove');
      },

      pass : function(){
        // pass should be made available only after a card is picked
        // pick new card
        var picked = fulldeck.pop();
        picked.faceup = true;
        discarded.unshift(picked);
        bus.$emit('pass', 'pass');
        
        // push to discard 
        // switchPlayer();
        this.endTurn();
        bus.$emit('calcNextMove', 'calcNextMove');
      },

      countPoints : function(){
        console.log("count points for player %o ", this.id );
        var doublePairs = countPlayerAdjacentDoublePairs(this.$data.hand);
        
        // doublePairs : each element for each double pair (4 cards) of same rank
        // [
        //    [Card(), Card(), Card(), Card()]
        // ]

        if(doublePairs && doublePairs.length>0){
          shared.doublePairs[this.id] = doublePairs.length;
        }
        // for(pr in pairs){
        //   var doublePairs = Math.floor(pairs[pr]/2);
        //   shared.doublePairs[this.id] = doublePairs;
        // }

        var playerPoints = countPlayerPoints(this.$data.hand);

        for(dp in shared.doublePairs){
          if (dp != this.id){
             var doublePairs = shared.doublePairs[dp];
            if(doublePairs){
              playerPoints += 10 * doublePairs;
            }
          }
        }

        return playerPoints;
      },

      endTurn : function(){
        if(this.countFaceDown()==0 && shared.gameEndedBy == ""){
          shared.gameEndedBy = this.id;
          this.gameEndedByMe = true;
        }

        if(shared.isLastMove){
          this.endGame();
          this.playedLastMove = true;
          shared.lastMovePlayedBy[this.id] = true;
        }

        bus.$emit('endTurn', 'endTurn');
        switchPlayer();
        bus.$emit('calcNextMove', 'calcNextMove');
      },

      addToQueue : function(){
        queue.add(1);
      },

      playNextMove: function(){
            
            var autoMove;
            if(this.nextMove && this.nextMove.hasOwnProperty('type')){
              autoMove = this.nextMove;
            }else{
              autoMove = this.myNextMove();
            }

            shared.turnLog.push(this.id + " - " + VALID_MOVES[autoMove.type]);
            
            console.log("auto play next move for " + this.id  + " " + autoMove.text )
            switch(autoMove.type){
              case MOVE_PICK_NEW:
                this.pickNewCard();
                this.lastMove=autoMove.text;
                // this.playNextMove();
                // ggjj
                this.nextMove = null;
                queue.add(this.id);
                break;
              case MOVE_DISCARD_FLIP:
                  discarded.unshift(picked);
                  console.log("discarded %o", discarded);
                  picked = null;
                  shared.picked = {};
                  bus.$emit('discarded', 'discarded');
                  this.autoFlipCard(autoMove.replace);
                  this.lastMove=autoMove.text;
                  this.nextMove = null;
                  // this.endTurn();
                break;
              case MOVE_DISCARD_ONLY:
                this.autoDiscardPicked();
                break;
              case MOVE_REPLACE:
                var pos = autoMove.replace.pos;
                this.autoSwapCard(autoMove.replace);
                this.hand[pos] = this.hand[pos];
                
                // this.autoSwapCard(autoMove.replace);
                this.$forceUpdate();
                this.lastMove = autoMove.text;
                console.log(this.id + " -> swap completed " +  autoMove.text)
                this.nextMove = null;
                // this.endTurn();
                break;
              case MOVE_OPEN_LAST_CARD:
                this.lastMove = autoMove.text;
                this.autoOpenLastCard();
                this.nextMove = null;
                break;
              default:
                console.err("cannot make move " + autoMove.text)
            }
    
            bus.$emit('calcNextMove', 'calcNextMove');
    
      },

      myNextMove: function(){
        console.group()
        var strategy = STRATEGY_OPEN_FIRST;

        console.log("nextMove for %o", this.id)
        var currentPoints = countPlayerPoints(this.hand);
        var isPicked = shared.picked.hasOwnProperty('card');
        var compareCard = isPicked?shared.picked:discarded[0];
        console.log("comare against : %o card : %o", (isPicked?'picked':'discarded'),compareCard.card);
        var calculatedMove = calcMove(compareCard, this);
        console.groupEnd();
        return calculatedMove;

        function calcMove(card, player){
            var move = "sgfs";
            var faceupCards = player.hand.filter(c => /*c.faceup*/ true);
            var possibleMoves = [];    
    
            function PossibleMove(type, replace, replaceWith, text){
              this.type = type;
              this.replace = replace;
              this.replaceWith = replaceWith;
              this.text = text;
            }

            var bestPossibleMove = getBestPossibleMove(faceupCards);
    
            function getBestPossibleMove(faceupCards){

              

                  for (i=0;i<faceupCards.length; i++){
      
                    var c = faceupCards[i];
                    
                    // console.log(" c : %o : %o", c.card , scores[c.rank]  );
                    // console.log(" card : %o : %o", card.card , scores[card.rank]  );
                   
                    // if (scores[card.rank]<scores[c.rank]){
                      // console.log(" replace" );
                      move = "replace " + (c.faceup?c.card:" facedown card at " + i ) + " with " + card.card;
                      possibleMoves.push(new PossibleMove(MOVE_REPLACE, c, card, move));
                    //}else{
                      // console.log(" dont replace" );
                    //}
                  }
          
                  var leastPoints = -999;
                  var bestPossibleMove = null;
          
                  for (i =0; i<possibleMoves.length; i++){
                      var considerMove = possibleMoves[i];
      
                      if(considerMove.type == MOVE_PICK_NEW){
                        continue;
                      }
                      var ficitiousHand = [];
                      var handCopy = JSON.parse(JSON.stringify(player.hand));
                      ficitiousHand = handCopy;
                      // swap card in fictitious hand
                      ficitiousHand.forEach(function(_c,_i,_arr){
                        if(_c.card == considerMove.replace.card){
                          _arr[_i] = JSON.parse(JSON.stringify(considerMove.replaceWith));
                        }
                      })
          
                      
                      var fictitiousPoints = countPlayerPoints(ficitiousHand);
                      if(leastPoints == -999){
                         leastPoints = fictitiousPoints;
                         bestPossibleMove = considerMove;
                      }else{
                        if(fictitiousPoints < leastPoints){
                          leastPoints = fictitiousPoints;
                          bestPossibleMove = considerMove;
                        }
                      }
      
                      considerMove.ficitiousHand = ficitiousHand;
                      considerMove.fictitiousPoints = fictitiousPoints;
                      considerMove.currentPoints = currentPoints;
                      considerMove.diff = fictitiousPoints - currentPoints;
                      considerMove.facedown = player.countFaceDown();
                      
                      //////////////////WARNING/////////////////////
                      //////////THIS CAUSES AN INFINITE LOOP ///////

                      // var myPos = considerMove.replace.pos;
                      // var pairCard = getPairOfIndexInHand(myPos, player.hand);
                      // var newCard = considerMove.replaceWith;
                      // if(pairCard.faceup == true){
                      //   if(newCard.rank = pairCard.rank){
                      //     console.log(considerMove.text + " will form a pair");
                      //     considerMove.formsPair = true;
                      //   }
                      // }
                      //////////THIS CAUSES AN INFINITE LOOP ///////
                      

                  }

                  return bestPossibleMove;
            }

            console.log("%o PossibleMoves for %o", possibleMoves.length, player.id);
            possibleMoves.forEach(function(m){
              console.log("%o : %o -> %o = %o. Facedown : %o" , m.text, m.currentPoints, m.fictitiousPoints, m.diff, m.facedown);
              var formsPair = willThisMoveFormAPair(m, player);
              m.formsPair = formsPair;
              if(formsPair){
                // console.log("Forms Pair : " + m.text);
                bestPossibleMove = m;
                bestPossibleMove.text = bestPossibleMove.text + " - Forms Pair ";
              }
            });
    
            var nextMove;
    
            // if(leastPoints > -999){
              if(bestPossibleMove.diff < 0){
                var m  = bestPossibleMove;
                console.log("Move with least points %o : %o -> %o = %o. Facedown : %o" , m.text, m.currentPoints, m.fictitiousPoints, m.diff, m.facedown);

                
                console.log("will this form a pair ? "  +  bestPossibleMove.formsPair);

                // switch only if there are less facedown cards than points to be gained by this switch
                if (bestPossibleMove.formsPair || Math.abs(m.diff) > m.facedown){
                  nextMove = bestPossibleMove;  
                }else{
                  if(!picked){
                  nextMove = new PossibleMove(MOVE_PICK_NEW, null, null, "Pick new card instead of ( " + m.text + " for " + m.diff + " points )" );  
                  }
                }
              }
            // }
    
            if(!nextMove){
              if(!isPicked){
                nextMove = new PossibleMove(MOVE_PICK_NEW, null, null, "Pick new card");  
              }

              if(isPicked){
                // DISCARD PICKED 
                // AND FLIP FACEDOWN CARD
                if(player.countFaceDown() > 1){
                  var facedowns = player.getFaceDownCards();
                  var randomFaceDown = getRandomElementFromArray(facedowns);
                  nextMove = new PossibleMove(MOVE_DISCARD_FLIP, randomFaceDown, null, "Discard and Flip card at " + randomFaceDown.pos);     
                }else{
                  
                  // just one card remaining 

                  nextMove = getBestPossibleMove(faceupCards);

                  var thisMoveWillEndTheGame = false;
                  if(nextMove.type == MOVE_REPLACE){
                    if(nextMove.replace.faceup == false){
                      thisMoveWillEndTheGame = true;
                    }
                  }
                  
                  // you always want lose points i.e. negative diff.
                  if(nextMove.diff > 0 ){
                    // i.e. this move will increase my points
                    // if(player.countPoints() < 3){
                    //   nextMove = new PossibleMove(MOVE_OPEN_LAST_CARD,null, null, "Open last card");  
                    // }else{
                      nextMove = new PossibleMove(MOVE_DISCARD_ONLY, null, null, "Discard Only");
                    // }
                  }else{
                    if(thisMoveWillEndTheGame){
                      if(nextMove.fictitiousPoints > 3){
                        nextMove = new PossibleMove(MOVE_DISCARD_ONLY, null, null, "Discard Only");
                      }else{
                        nextMove = new PossibleMove(MOVE_OPEN_LAST_CARD,null, null, "Open last card");  
                      }
                    }
                  }



                  // TODO - nextMove is now the move with least points
                  // 1. Check that nextMove does not increase points
                  // i.e. if nextMove.diff is > 0, then do not swap,

                  // instead choose either to pass or end game by flipping last card.
                  // 2. Allow pass option 
                  // 3. Allow end game option
                }
                

                // OR SWAP FACEUP CARD
                // 
              }
            }
    
            
            if(nextMove){
              console.log("BestMove " + nextMove.text);
              console.log(nextMove);
              // return nextMove.text;
              return nextMove;
            }
            console.groupEnd();
            throw "NO NEXT MOVE !";
            }
      },

      autoFlipCard : function(cardToFlip){
        cardToFlip.flipMode = true;
        return this.flipCard(cardToFlip);
      },

      flipCard : function(cardToFlip){
        if(!cardToFlip.flipMode){
          return;
        }
        console.log("flipping")
        bus.$emit('exitFlipMode',cardToFlip);
        cardToFlip.faceup = true;
        bus.$emit('flipCompleted',cardToFlip);
      },
      autoSwapCard : function(selectedCard){
        selectedCard.swapMode = true;
        return this.swapCard(selectedCard);
      },
      swapCard: function(selectedCard){
      if(!selectedCard.swapMode){
        return;
      }
      console.log("swapping")
      bus.$emit('exitSwapMode', selectedCard);
      
      console.log(selectedCard);
      console.log("hand before swap")
      console.log(this.hand.map(c => c.card));
      selectedCardIndex = selectedCard.pos;
      if(shared.picked.hasOwnProperty('card')){
        shared.picked.player = selectedCard.player;
        var a = JSON.stringify(selectedCard);
        var b = JSON.stringify(shared.picked);
        selectedCard = JSON.parse(b);
        selectedCard.pos = selectedCardIndex;
        // this.hand[selectedCardIndex] = selectedCard;
        Vue.set(this.hand,selectedCardIndex, selectedCard);
        // discarded.pop();
        var _discard = JSON.parse(a);
        _discard.player = "";
        _discard.pos = -1;
        discarded.unshift(_discard);
        discarded[0].faceup = true;
        shared.picked = {};
      }else{
        discarded[0].player = selectedCard.player;
        var a = JSON.stringify(selectedCard);
        var b = JSON.stringify(discarded[0]);
        selectedCard = JSON.parse(b);
        selectedCard.pos = selectedCardIndex;
        // this.hand[selectedCardIndex] = selectedCard;
        Vue.set(this.hand,selectedCardIndex, selectedCard);
        // discarded.pop();
        var _discard = JSON.parse(a);
        _discard.player = "";
        _discard.pos = -1;
        discarded.unshift(_discard);
        discarded[0].faceup = true;
      }

      console.log("hand after swap")
      console.log(this.hand.map(c => c.card));
      if(this.countFaceDown() == 0){
        shared.isLastMove= true;
      }
      this.endTurn();
      // bus.$emit('swapCompleted' ,{
      //   in: selectedCard,
      //   out : discarded[0],
      //   model : selectedCard,
      // })

      return selectedCard;

    }
    },

    created : function(){
          
      },

      mounted : function(){
        console.log('mounted');
        console.log(this);
        var self = this;

        bus.$on('exitSwapMode', function(){
          console.log('on swapped');
          self.exitSwapMode();
        })

        bus.$on('exitFlipMode', function(){
          console.log('on flipped');
          self.exitFlipMode();
        })

        bus.$on('calcNextMove', function(){
          var x = self.myNextMove();
          console.log("#####################################")
          console.log(x.text)
          self.nextMove = x;
        });

        bus.$on('flipCompleted', function(d){
           if(d.player != self.id){
            console.log("not my event");
            return;
          }
          if(self.countFaceDown()==0){
            // bus.$emit('endgame','endgame');
            // bus.$emit('isLastMove','isLastMove');
            shared.isLastMove = true;
          }else{
            // switchPlayer();  
            self.endTurn();
          }
          
        });


        bus.$on('swapCompleteda', function(d){
          console.log('on swap completed %o', d.model.player);
          console.log('player %o', self.id);
          if(d.model.player != self.id){
            console.log("not my event");
            return;
          }
          console.log(self.hand);
          console.log("added " + d.in.card);
          console.log("removed " + d.out.card);

          // TODO why not just use the position of the card to replace?
          // self.hand.forEach(function(s, ind, arr){
          //   if(s.card == d.out.card){
          //     arr[ind]= d.in;
          //   }
          // })
          self.hand[d.out.pos] = d.in;
          self.hand.forEach(function(s){if(s.faceup){console.log(s.card)}})

          if(self.countFaceDown()==0){
            // bus.$emit('endgame','endgame');
            shared.isLastMove = true;
          }
          // else{
            // switchPlayer();  
            self.endTurn();
          // }
        })

        bus.$on('endgame',function(){
          console.log("game ended by " + self.id);
          self.endGame();
        })
      }
  })
}

function Pair(card1, card2){
    this.card1=card1;
    this.card2=card2;
  }


function countPlayerAdjacentDoublePairs(_hand){
  var doublePairs = [];
  var doublePairPos = []; // track cards already counted in a double pair
  var patterns = shared.doublePairIndexPatterns;
  if(!patterns){
    return;
  }
  
  var hand = _hand.map(c => c);

  patterns.forEach(function(patternArr){
    
    // patterns has an array of positions
    // get all cards at those positions
    var cardsAt = [];
    for(i=0; i<patternArr.length;i++){
      var index = patternArr[i];
      if(hand[index].faceup){
        cardsAt.push(hand[index]);
      }
    }
     // = patternArr.map(i => opencards[i]);

    // now check if all cards in pattern have the same rank
    if(cardsAt.length == patternArr.length){
      var hasDoublePair = cardsAt.every((c,i,arr) => {
        return c.faceup // carh should be open
        && c.rank == arr[0].rank // all cards must be same
        && !doublePairPos.includes(c.pos) // should not already be counted
      });

      if(hasDoublePair){
        doublePairs.push(cardsAt);
        cardsAt.forEach(c => {doublePairPos.push(c.pos)})
        // remove doublepairs from opencards
      }
    }
  })

  return doublePairs;
}


function countPlayerPoints(hand){
  
  var total = 0;
  hand.forEach(function(c){
    if(c.faceup){
      var p = scores[c.rank];
      total += p;
      // console.log(" added %o for %o. total is %o", p, c.card, total);
    }
  })
  var deductions = 0;

  

  var offset =hand.length/2; 
  for(x =0; x<offset; x++){
    if(hand[x].faceup && hand[x+offset].faceup){
      if(hand[x].rank == hand[x+offset].rank){

        var d = scores[hand[x].rank] * 2;
        if(d>0){
          deductions += d;
          // console.log(" deduction %o for pair (%o, %o)", d, hand[x].card, hand[x+offset].card);
        }else{
          // console.log("no deduction for pair (%o, %o) ", hand[x].card, hand[x+offset].card)
        }
      }

      if(hand[x].rank == JOKER.rank || hand[x+offset].rank ==JOKER.rank){
        var jd = 0;
        jd += scores[hand[x].rank];
        jd += scores[hand[x+offset].rank];
        deductions += jd;
        // console.log(" deduction %o for joker pair (%o, %o)", jd, hand[x].card, hand[x+offset].card);
      }
    }
  }

  
  // console.log("total -> " + total);
  // console.log("deduction -> " + deductions);
  // console.log("final -> " + (total - deductions));
  return total - deductions;
}


// var player2 = new Vue({
//   el: '#player2',
//   data: {
//     hand : randomCards(6)/*.map(c=>{c.faceup=true; return c})*/,
//     cols : 3,
//   }
// })

function getNewHandForPlayer(count, id){
  return getNewHand(count).map((c, ind) => {c.player = id; c.pos = ind; return c;});
}

function getNewHand(count){
  var drawnCards = randomCards(count);
  console.log("drawnCards");
  console.log(drawnCards);
  var faceupIndex = getRandomIntInclusive(0, count-1);
  console.log(faceupIndex)
  console.log(drawnCards[faceupIndex])
  drawnCards[faceupIndex].faceup = true;
  return drawnCards;
}



function randomCards(count){
  var v = [];
  for(i=0;i<count; i++){
    var randomIndex = getRandomIntInclusive(0, fulldeck.length-1);
    // console.log(randomIndex);
    // console.log(fulldeck[randomIndex]);
    var pickedCard = fulldeck.splice(randomIndex, 1)
    // console.log(pickedCard);
    v.push(pickedCard[0]);

    // v.push(fulldeck.splice(getRandomIntInclusive(0, 51), 1));
  }

  return v;
}

function getPairOfIndexInHand(x, hand){
  if(x >= hand.length/2){
    return hand[x-hand.length/2]
  } else {
    return hand[x+hand.length/2]
  } 
}

function willThisMoveFormAPair(considerMove, player){
    var myPos = considerMove.replace.pos;
    var pairCard = getPairOfIndexInHand(myPos, player.hand);
    var newCard = considerMove.replaceWith;
    if(pairCard.faceup){
      if(newCard.rank == pairCard.rank){
        console.log(considerMove.text + " will form a pair (%o, %o)",newCard.card, pairCard.card);
        return true;
      }
    }
    return false;
  };

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
  }

function getRandomElementFromArray(arr) {
  console.log("getRandomElementFromArray");
  var r = getRandomIntInclusive(0, arr.length-1);
  console.log("random elem -> " + r)
    return arr[r];
  }

function switchPlayer(){

  console.log('shared.player')
  console.log(shared.player);
  if(!shared.player || shared.player == "" || shared.player.trim().length < 1){
    shared.player = shared.players[0].id;
    queue.add(shared.player);
    // shared.players[0].playNextMove();
    // ggjj
    return;
  }
  for(i = 0; i<shared.players.length; i++){
    if(shared.players[i].id == shared.player){
      if(shared.players[i].countFaceDown()==0){
        
        // if(shared.players[i].playedLastMove)
        if(Object.keys(shared.lastMovePlayedBy).length==shared.players.length){
          bus.$emit('endgame', 'endgame');
          bus.$emit('gameover', 'gameover');
          clearInterval(queue.loop);
          shared.player = "";
          break;
        }
        // shared.player = "";
        // break;
      } 
      var nextPlayer = null;
      if(i==shared.players.length-1){
        nextPlayer = shared.players[0];
        shared.player = nextPlayer.id;
      }else{
        nextPlayer = shared.players[i+1];
        shared.player = nextPlayer.id;
      }
      if(nextPlayer.hasOwnProperty('id')){
        // nextPlayer.playNextMove();
        // ggjj
        queue.add(nextPlayer.id);
      }
      break;
    }
  }
  console.log("switch player ", shared.player);
}

function switchPlayer2(){

  console.log('shared.player')
  console.log(shared.player);
  if(shared.player=='1'){
    console.log('set player 2 ')
    shared.player ='2';
  }else{
    console.log('set player 1 ')
    shared.player ='1' ;
  }
  console.log("switch player ", shared.player);
}


console.log("fulldeck %o" , fulldeck);


t = fulldeck.pop();
console.log("fulldeck %o" , fulldeck);
console.log("top %o" , t);
t.faceup = true;
shared.discarded.push(t);
Vue.set(shared,discarded, shared.discarded);
console.log("discarded %o", discarded);
picked = fulldeck.pop();
picked.faceup = true;
console.log("picked %o", picked);
switchPlayer();
bus.$emit('calcNextMove', 'calcNextMove');
fulldeck.forEach(c=> c.faceup = true)
